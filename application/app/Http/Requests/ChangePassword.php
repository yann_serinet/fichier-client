<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{

    protected $errorBag = 'changePassword';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => [
                'required',
                'max:100',
                'min:5',
                'same:password'
            ],
            'password_confirmation' => [
                'required',
                'max:100',
                'min:5',
                'same:password'
            ],
        ];
    }
}
