<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdmin extends FormRequest
{

    protected $errorBag = 'storeAdmin';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => [
                'required',
                'max:100',
            ],
            'name' => [
                'required',
                'max:100',
            ],
            'email' => [
                'required',
                'max:100',
            ],
        ];
     }
}
