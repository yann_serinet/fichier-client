<?php

namespace App\Http\Controllers;

use App\Forms\Competition\defaultForm;
use App\Http\Requests\StoreCompetition;
use App\Models\Competition;
use Illuminate\Http\Request;

class CompetitionsController extends Controller
{

    public function index()
    {
        $this->data['competitions'] = Competition::all();
        return $this->view();
    }


    public function create()
    {
        $competition = new Competition();
        $this->data['competition'] = $competition;
        $form = new defaultForm(route('admin.competitions.store'), $competition,'POST');
        $this->data['form'] = $form->renderForm();
        $this->data['title'] = 'Ajouter une nouvelle compétition';
        return $this->view();
    }

    public function store(StoreCompetition $request)
    {
        $fields = $request->all();
        $competition = Competition::create($fields);
        if(!$competition){
            flash('error when create competition')->error()->important();
            return back()->withInput();
        } else {
            flash('Successfully creating')->success()->important();
        }

        return redirect(route('admin.competitions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $competition = Competition::findOrFail($id);
        $this->data['competition'] = $competition;
        $form = new defaultForm(route('admin.competitions.update', ['id' => $competition->id]), $competition, 'PUT');
        $this->data['form'] = $form->renderForm();
        $this->data['title'] = 'Edition de la compétition';
        return $this->view();
    }

    /**
     * Update in database
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(StoreCompetition $request, $id)
    {
        $competition = Competition::findOrFail($id);
        $fields = $request->all();
        $competition->fill($fields);
        if($competition->save()) {
            flash('Successfully update competition!')->success()->important();
        } else {
            flash('error when update competition !')->error()->important();
        }

        return redirect(route('admin.competitions.index'));
    }



    /**
     * Remove from database
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $competition = Competition::find($id);
        if($competition->delete()) {
            flash('Successfully destroy competition !')->success()->important();
        } else {
            flash('Error when destroyed competition !')->error()->important();
        }

        return redirect(route('admin.competitions.index'));
    }



    public function travels(Request $request, $id)
    {
        $competition = Competition::find($id);
        $this->data['competition'] = $competition;
        $this->data['travels'] = $competition->travels()->get();

        $this->data['count_participants'] = 32;

        return $this->view();
    }
}

