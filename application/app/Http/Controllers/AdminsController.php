<?php

namespace App\Http\Controllers;

use App\Forms\Admin\defaultForm;
use App\Forms\Admin\passwordForm;
use App\Http\Requests\ChangePassword;
use App\Http\Requests\StoreAdmin;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminsController extends Controller
{

    public function store(StoreAdmin $request)
    {

        $fields = $request->all();
        $clear_password = str_random(8);
        $fields['password'] = Hash::make($clear_password);
        $new_admin = Admin::create($fields);

        if(!$new_admin){

            Mail::to($new_admin->email)->send(new OrderShipped($clear_password));
            flash('error when create admin')->error()->important();
            return back()->withInput();
        } else {
            flash('Successfully creating')->success()->important();
        }

        return redirect(route('admin.admins.index'));
    }



    /**
     * Show the form for editing
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $default_form = new defaultForm(route('admin.admins.update', ['id' => $admin->id]), $admin, 'PUT');
        $password_form = new passwordForm(route('admin.admins.update_password', ['id' => $admin->id]), $admin, 'PUT');
        $this->data['default_form'] = $default_form->renderForm();
        $this->data['password_form'] = $password_form->renderForm();
        return $this->view();
    }

    /**
     * Update in database
     *
     * @param StoreAdmin $request
     * @param  int  $id
     * @return Response
     */
    public function update(StoreAdmin $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $fields = $request->all();
        $admin->fill($fields);
        if($admin->save()) {
            flash('Successfully update admin!')->success()->important();
        } else {
            flash('error when update admin !')->error()->important();
        }

        return redirect(route('admin.admins.index'));
    }


    public function update_password(ChangePassword $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $admin->password = Hash::make($request->password);
        if($admin->save()) {
            flash('Successfully update admin password !')->success()->important();
        } else {
            flash('error when update admin password!')->error()->important();
        }

        return redirect(route('admin.admins.index'));
    }

    /**
     * Remove from database
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $admin = Admin::find($id);
        if($admin->delete()) {
            flash('Successfully destroy admin !')->success()->important();
        } else {
            flash('Error when destroyed admin !')->error()->important();
        }

        return redirect(route('admin.admins.index'));
    }
}

