<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data;

    protected $redirect_to;

    protected $model;
    protected $model_namespaced;
    protected $controller;

    /*
    public function __construct(Request $request)
    {
        $this->data = array();
        $this->data['css']['class'] = 'form-control';

        $action = $request->route()->getAction();
        $tmp = explode('\\', $action['controller']);
        $tmp2 = explode('@', array_pop($tmp));
        $this->model = str_replace('sController', '',$tmp2[0]);
        $this->model_namespaced = '\\App\Models\\'.$this->model;
        $this->model = strtolower($this->model);

        Session::put('errors',!is_null(Session::get('errors'))?: new ViewErrorBag);
        $this->middleware(function ($request, $next) {
            $this->data['admin'] = Auth::guard('admin')->user();
            return $next($request);
        });
        $this->redirect_to = route('admin.index.index');
    }
    */

    /**
     * @param array $data
     * @param string $view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($data = array(), $view = '')
    {
        if(sizeof($this->data) == 0){
            $this->data = $data;
        }
        if($view == ''){
            $view = strtolower(\Route::currentRouteName());


            if(strpos($view, '.create') !== false && !View::exists($view)){
                $view = str_replace('.create', '.form', $view);
            }
            if(strpos($view, '.edit') !== false && !View::exists($view)){
                $view = str_replace('.edit', '.form', $view);
            }
        }
        return view($view, $this->data);
    }


    public function index(Request $request)
    {
        $this->data[$this->model.'s'] = $this->model_namespaced::all();
        return $this->view();
    }


    public function create()
    {
        $model = new $this->model_namespaced();
        $form_model = 'App\Forms\\'.ucfirst($this->model).'\defaultForm';
        $default_form = new $form_model(route('admin.'.$this->model.'s.store'), $model, 'POST');
        $this->data['default_form'] = $default_form->renderForm();
        return $this->view();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }
}
