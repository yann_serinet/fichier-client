<?php

namespace App\Forms\Competition;

use App\Forms\Form;
use App\Models\CompetitionType;

class defaultForm extends Form
{

    /**
     * defaultForm constructor.
     * @param string $url
     * @param Illuminate\Database\Eloquent\Model $model
     * @param string $method
     * @param bool $files
     */
    public function __construct($url, $model, $method = 'POST', $files = false)
    {

        parent::__construct($url, $model, $method, $files);
        $this->error_bag = 'storeCompetition';
    }

    public function getFields()
    {
        return [
            'active' => [
                'label' => 'Activer',
                'value' => (bool)$this->model->active,
                'type' => 'checkbox',
            ],
            'title' => [
                'label' => 'Titre',
                'value' => $this->model->title,
                'type' => 'text',
                'placeholder' => 'Le Titre'
            ],
            'begin_date' => [
                'label' => 'Date de debut',
                'value' => $this->model->begin_date,
                'type' => 'date',
                'placeholder' => 'Date de Début'
            ],
            'end_date' => [
                'label' => 'Date de fin',
                'value' => $this->model->end_date,
                'type' => 'date',
                'placeholder' => 'Date de fin'
            ],
            'country' => [
                'label' => 'Pays',
                'value' => $this->model->country,
                'type' => 'text',
                'placeholder' => 'Pays'
            ],
            'content' => [
                'label' => 'Genre',
                'value' => $this->model->sex,
                'collection' => ['M' => 'Masculin', 'F' => 'Feminin'],
                'type' => 'select',
            ],
            'competition_type_id' => [
                'label' => 'Type de Compétition',
                'value' => $this->model->competition_type_id,
                'collection' => CompetitionType::all()->pluck('title', 'id'),
                'type' => 'select',
            ]
        ];
    }
}