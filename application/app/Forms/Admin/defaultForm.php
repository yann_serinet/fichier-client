<?php

namespace App\Forms\Admin;

use App\Forms\Form;
use App\Models\Region;

class defaultForm extends Form
{

    /**
     * defaultForm constructor.
     * @param string $url
     * @param Illuminate\Database\Eloquent\Model $model
     * @param string $method
     * @param bool $files
     */
    public function __construct($url, $model, $method = 'POST', $files = false)
    {

        parent::__construct($url, $model, $method, $files);
        $this->error_bag = 'storeAdmin';
    }

    public function getFields()
    {
        return [
            'firstname' => [
                'label' => 'Prénom',
                'value' => $this->model->firstname,
                'type' => 'text',
                'placeholder' => 'Le Prénom'
            ],
            'name' => [
                'label' => 'Nom',
                'value' => $this->model->name,
                'type' => 'text',
                'placeholder' => 'Le Nom'
            ],
            'email' => [
                'label' => 'Mail',
                'value' => $this->model->email,
                'type' => 'text',
                'placeholder' => 'Le Mail'
            ]
        ];
    }
}