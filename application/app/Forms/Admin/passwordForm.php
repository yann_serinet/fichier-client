<?php

namespace App\Forms\Admin;

use App\Forms\Form;
use App\Models\Region;

class passwordForm extends Form
{

    /**
     * defaultForm constructor.
     * @param string $url
     * @param Illuminate\Database\Eloquent\Model $model
     * @param string $method
     * @param bool $files
     */
    public function __construct($url, $model, $method = 'POST', $files = false)
    {

        parent::__construct($url, $model, $method, $files);
        $this->error_bag = 'ChangePassword';
    }

    public function getFields()
    {
        return [
            'password' => [
                'label' => 'Mot de passe',
                'value' => '',
                'type' => 'password',
                'placeholder' => 'Saisir le nouveau mot de passe'
            ],
            'password_confirmation' => [
                'label' => 'Mot de passe',
                'value' => '',
                'type' => 'password',
                'placeholder' => 'Saisir à nouveau le mot de passe'
            ]
        ];
    }
}