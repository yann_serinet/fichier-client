<?php

namespace App\Forms\Travel;

use App\Forms\Form;
use App\Models\Competition;
use App\Models\CompetitionType;

class defaultForm extends Form
{

    /**
     * defaultForm constructor.
     * @param string $url
     * @param Illuminate\Database\Eloquent\Model $model
     * @param string $method
     * @param bool $files
     */
    public function __construct($url, $model, $method = 'POST', $files = false)
    {

        parent::__construct($url, $model, $method, $files);
        $this->error_bag = 'storeTravel';
    }

    public function getFields(){
        return [
            'active' => [
                'label' => 'Activer ?',
                'value' => $this->model->active,
                'type' => 'checkbox',
                'placeholder' => 'Activer ?'
            ],
            'title' => [
                'label' => 'Titre',
                'value' => $this->model->title,
                'type' => 'text',
                'placeholder' => 'Le Titre'
            ],
            'content' => [
                'label' => 'Contenu du Voyage',
                'value' => $this->model->content,
                'type' => 'text',
                'placeholder' => 'Le Titre'
            ],
            'begin_date' => [
                'label' => 'Date de début du voyage',
                'value' => $this->model->begin_date,
                'type' => 'date',
                'placeholder' => 'Date de début'
            ],
            'end_date' => [
                'label' => 'Date de fin du voyage',
                'value' => $this->model->end_date,
                'type' => 'date',
                'placeholder' => 'Date de fin'
            ],
            'competition_id' => [
                'label' => 'Compétition',
                'value' => $this->model->competition_id,
                'collection' => Competition::all()->pluck('title', 'id'),
                'type' => 'select',
            ]
        ];
    }
}