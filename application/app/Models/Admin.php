<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;


    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFormFields(){
        return [
            'firstname' => [
                'label' => 'Prénom',
                'value' => $this->firstname,
                'type' => 'text',
                'placeholder' => 'Le Prénom'
            ],
            'name' => [
                'label' => 'Nom',
                'value' => $this->name,
                'type' => 'text',
                'placeholder' => 'Le Nom'
            ],
            'email' => [
                'label' => 'Mail',
                'value' => $this->email,
                'type' => 'text',
                'placeholder' => 'Le Mail'
            ]
        ];
    }
}
