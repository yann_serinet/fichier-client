<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id)
            ->latest();
    }

    public function scopeLast5($query)
    {
        return $query->limit(5);
    }
}
