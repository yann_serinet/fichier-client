<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'name', 'email', 'password', 'birthdate', 'street1', 'street2', 'postal_code', 'city', 'region_id',
        'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getFullnameAttribute()
    {
        return $this->firstname.' '. $this->name;
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }


    public function scopeOfAdmin($query, $admin)
    {
        if($admin->region_id !=0) {
            return $query->where('region_id', $admin->region_id);
        } else {
            return $query;
        }
    }
}
