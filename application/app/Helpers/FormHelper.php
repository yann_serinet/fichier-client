<?php

namespace App\Helpers;

use Collective\Html\FormFacade;

class FormHelper
{

    /**
     * @param string $name (name pour l'input)
     * @param string $title (titre de l'input dans le label)
     * @param string $value (valeur de l'input)
     * @param string $type (text, date, number, hidden, password)
     * @param string $placeholder
     * @return string
     */
    public static function formGroupInput($name, $title, $value, $type, $placeholder)
    {
        $string = '<div class="form-group">';
        $string .= FormFacade::label($name, $title);

        $string .= '';
                  //data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""
        if($type == 'file') {
            $string .= FormFacade::file($name);
        } elseif($type == 'date') {
            $string .= '<div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>'.
                FormFacade::$type($name, $value, ['id' => $name, 'class' => 'form-control', 'placeholder' => $placeholder])
            .'</div>';
        } else {
            $string .= FormFacade::$type($name, $value, ['id' => $name, 'class' => 'form-control', 'placeholder' => $placeholder]);
        }


        $string .= '</div>';
        return $string;
    }



    /**
     * @param string $name (name pour l'input)
     * @param string $title (titre de l'input dans le label)
     * @param string $placeholder
     * @return string
     */
    public static function formGroupPassword($name, $title, $placeholder)
    {
        $string = '<div class="form-group">';
        $string .= FormFacade::label($name, $title);
        $string .= FormFacade::password($name, ['id' => $name, 'class' => 'form-control', 'placeholder' => $placeholder]);
        $string .= '</div>';
        return $string;
    }


    /**
     * @param string $name (name pour l'input)
     * @param string $title (titre de l'input dans le label)
     * @param bool $checked
     * @param string $label
     * @param string $value
     * @param bool $invers (inverser checkbox et label)
     * @return string
     */
    public static function formGroupCheckbox($name, $title, $checked, $label = '', $value = 1, $invers = false)
    {
        $label = ($label != '')? $label : $name;

        $string = '<div class="form-group">';

        $string .= '<label for="'.$label.'">'.((!$invers)?$title.'&nbsp;&nbsp;&nbsp;&nbsp;':'');

        $string .= FormFacade::checkbox($name, $value, $checked, ['id' => $name, 'class' => 'minimal']);

        $string .= (!$invers)?'':'&nbsp;&nbsp;&nbsp;'.$title;

        $string .= '</label></div>';
        return $string;
    }


    /**
     * @param string $name
     * @param string $title
     * @param string $value
     * @param Collection $collection
     * @return string
     */
    public static function formGroupSelect($name, $title, $value, $collection)
    {
        $string = '<div class="form-group">';
        $string .= FormFacade::label($name, $title);
        $string .= FormFacade::select($name, $collection ,$value, ['class' =>'form-control']);
        $string .= '</div>';

        return $string;
    }
}