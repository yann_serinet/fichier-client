<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TestsBegin extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    protected $cycle;


    /**
     * Create a new message instance.
     * @param string $message
     * @param Cycle $cycle
     * @return void
     */
    public function __construct($message, Cycle $cycle)
    {
        $this->message = $message;
        $this->cycle = $cycle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.tests.begin')
                     ->text('emails.tests.begin_plain')
                     ->with([
                         'message_test' => $this->message,
                         'cycle' => $this->cycle,
                    ]);
    }
}
