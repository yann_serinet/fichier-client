<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Admin', 'prefix' => 'administration', 'as'=>'admin.'], function () {

    Auth::routes();

    Route::group(['middleware' => 'auth:admin'], function () {

        Route::get('/', 'IndexController@index')->name('index.index');
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

        Route::resource('admins', 'AdminsController');
        Route::put('/admins/update_password/{id}', 'AdminsController@update_password')->name('admins.update_password');

    });
});


Auth::routes();
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.callback_provider');


Route::group(['middleware' => 'auth:web'], function () {
    Route::view('/', 'index');
});

Route::get('/test', 'DashboardController@testBackofficeConnection');
Route::get('/{index?}', 'DashboardController@index');


Route::get('/algorithme', 'DashboardController@algorithme');
