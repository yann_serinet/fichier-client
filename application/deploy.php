<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'handball-events');

// Project repository
set('repository', 'git@bitbucket.org:yann_serinet/association_hanbdall.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('user', 'deploy');
set('git_tty', true);
set('ssh_multiplexing', false);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);

set('keep_releases', 10);

// Hosts
host('37.187.101.10')
    ->stage('production')
    ->forwardAgent()
    ->set('deploy_path', '/home/yann/handball-events');
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

set('slack_text', '{{user}} deploying {{branch}} to {{hostname}}');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

