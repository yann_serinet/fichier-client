@if($card->enquirer )
    @if ($card->enquirer->acces_user)
	@if($card->enquirer && $card->enquirer->acces_user->url_image)
	    <img src="/maxigestion/admin/img/equipe/{{ $card->enquirer->acces_user->url_image }}" title="{{$card->enquirer->acces_user->full_name}}" class="enquirer-pic">
	@else
	    <img src="{{ asset('img/profile_placeholder.png') }}" title="{{$card->enquirer->acces_user->full_name}}" class="enquirer-pic">
	@endif
    @else
	<img src="{{ asset('img/profile_placeholder.png') }}" title="{{ $card->enquirer->email }}" class="enquirer-pic">
    @endif
@else
    <img src="{{ asset('img/profile_placeholder.png') }}" title="Utilisateur introuvable" class="enquirer-pic">
@endif
    <!-- todo text -->
<span class="text">#{{$card->number}} - <a href="{{ \App\Services\AxosoftService::AXOSOFT_URL }}viewitem?id={{ $card->number }}&type={{ $card->type }}&force_use_number=true" target="_blank">{{ $card->name }}</a></span>
    <!-- Emphasis label -->
    <span class="label label-danger"><i class="fa fa-clock-o"></i> {{$card->estimated_duration}}</span>
    <!-- General tools such as edit or delete-->
