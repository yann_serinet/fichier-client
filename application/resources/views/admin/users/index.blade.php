@extends('admin.layouts.app')

@section('title', 'Liste des Adhérents')
@section('subtitle', '')


@section('content')


    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>150</h3>

                    <p>New Orders</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>53<sup style="font-size: 20px">%</sup></h3>

                    <p>Bounce Rate</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>44</h3>

                    <p>User Registrations</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>65</h3>

                    <p>Unique Visitors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Les Adhérents</h3>
                    <a href="{{route('admin.users.create')}}" class="pull-right btn btn-success"><i class="fa fa-plus"></i></a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th>email</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Région</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{$user->email}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->firstname}}</td>
                                <td>{{$user->region->title}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{route('admin.users.edit', ['id' => $user->id])}}"><i
                                                            class="fa fa-edit"></i>Modifier</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{route('admin.users.achats', ['id' => $user->id])}}"><i
                                                            class="fa fa-credit-card"></i>Voir ses achats</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{route('admin.users.accounts', ['id' => $user->id])}}"><i
                                                            class="fa fa-users"></i>Voir les comptes associés</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="$(this).find('form').submit();">
                                                    {{ Form::open(array('url' => route('admin.users.destroy', ['id' => $user->id]))) }}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::close() }}
                                                    <i class="fa fa-trash-o"></i>Supprimer
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    <!-- /.col -->
    </div>


@endsection