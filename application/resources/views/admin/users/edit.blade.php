@extends('admin.layouts.app')

@section('title', 'Edition du profil adhérent')
@section('subtitle', 'V1.0')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Informations générales</h3>
				</div>
				@include('admin.users._form', ['url' => route('admin.users.update', ['id' => $user->id]), 'method' => 'PUT'])

			</div>
		</div>

		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Changer de mot de passe</h3>
				</div>
				@include('admin.users._change_password', ['url' => route('admin.users.update_password', ['id' => $user->id]), 'method' => 'PUT'])

			</div>
		</div>

		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Intéressé par</h3>
				</div>
				@include('admin.users._change_interests', ['url' => route('admin.users.update_interests', ['id' => $user->id]), 'method' => 'PUT'])

			</div>
		</div>
	</div>

@endsection