@extends('admin.layouts.app')

@section('title', 'Ajouter un nouvel adhérent')
@section('subtitle', 'V1.0')


@section('content')

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
			@include('admin.users._form', ['url' => route('admin.users.store'), 'method' => 'POST'])
			</div>
		</div>
	</div>
@endsection