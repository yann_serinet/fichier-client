{{ Form::open(array('url' => $url, 'method' => $method, 'files' => true)) }}
<div class="box-body">
    @if (count($errors->storeAdmin) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->storeAdmin->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        {!! FormHelper::formGroupInput('firstname', 'Prénom', $user->firstname, 'text', 'Prénom') !!}
        {!! FormHelper::formGroupInput('name', 'Nom', $user->name, 'text', 'Le Nom') !!}
        {!! FormHelper::formGroupInput('email', 'Email', $user->email, 'email', 'Le mail') !!}
        {!! FormHelper::formGroupInput('birthdate', 'Date de Naissance', $user->birthdate, 'date', '') !!}
        {!! FormHelper::formGroupInput('street1', 'Adresse', $user->street1, 'text', 'Adresse') !!}
        {!! FormHelper::formGroupInput('street2', 'Complément d\'adresse', $user->street2, 'text', 'Complément') !!}
        {!! FormHelper::formGroupInput('postal_code', 'Code Postal', $user->postal_code, 'text', 'Code Postal') !!}
        {!! FormHelper::formGroupInput('city', 'Ville', $user->city, 'text', 'Ville') !!}
        {!! FormHelper::formGroupSelect('region_id', 'Région', $user->region_id, $regions) !!}
        {!! FormHelper::formGroupInput('user_id', 'Associé à', $user_id, 'text', '') !!}

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Enregistrer</button>
</div>
{{ Form::close() }}