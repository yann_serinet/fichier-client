{{ Form::open(array('url' => $url, 'method' => $method, 'files' => true)) }}
<div class="box-body">
    @if (count($errors->changeInterests) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->changeInterests->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @foreach($competition_types as $type)
        {!! FormHelper::formGroupCheckbox('types[]',$type->title, $user->competitionTypes->contains($type->id), $type->id, $type->id, true) !!}
    @endforeach

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Enregistrer</button>
</div>
{{ Form::close() }}