@extends('admin.layouts.app')

@section('title', 'Liste des Achats de ')
@section('subtitle', '')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Les Adhérents</h3>
                    <a href="{{route('admin.users.create')}}" class="pull-right btn btn-success"><i class="fa fa-plus"></i></a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th>email</th>
                            <th>Nom</th>
                            <th>Région</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{$user->email}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->region->title}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{route('admin.users.edit', ['id' => $user->id])}}"><i
                                                            class="fa fa-edit"></i>Modifier</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{route('admin.users.achats', ['id' => $user->id])}}"><i
                                                            class="fa fa-credit-card"></i>Voir ses achats</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="$(this).find('form').submit();">
                                                    {{ Form::open(array('url' => route('admin.users.destroy', ['id' => $user->id]))) }}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::close() }}
                                                    <i class="fa fa-trash-o"></i>Supprimer
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    <!-- /.col -->
    </div>


@endsection