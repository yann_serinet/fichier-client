{{ Form::open(array('url' => $url, 'method' => $method, 'files' => true)) }}
<div class="box-body">
    @if (count($errors->changePassword) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->changePassword->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Saisir le nouveau mot de passe" value="">
    </div>

    <div class="form-group">
        <label for="password_confirmation">Mot de passe </label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Saisir à nouveau le mot de passe " value="">
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Enregistrer</button>
</div>
{{ Form::close() }}