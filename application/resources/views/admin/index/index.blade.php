@extends('admin.layouts.app')

@section('title', 'Index Index')
@section('subtitle', 'V1.0')

@section('content')
	<div class="row dashboard-admin-row">
		<div class="col-lg-7 col-md-12">
			<div class="row">
			</div>
			<div class="row">
			</div>
		</div>
		<div class="col-lg-5 col-md-12 height-limited">

			<div class="box box-primary backlog-box">
				<div class="box-header">
					<i class="ion ion-clipboard"></i>

					<h3 class="box-title">Backlog</h3>
					<div class="box-tools">
						<div class="form-group backlog-filter">
							<label>
								<input type="radio" name="backlog_filter" value="features" checked>
								Features
							</label>
							<label>
								<input type="radio" name="backlog_filter" value="defects">
								Bugs
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col -->
		<div class="col-lg-7 col-md-12">
		</div>
	</div>
@endsection