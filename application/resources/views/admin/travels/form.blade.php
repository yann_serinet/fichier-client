@extends('admin.layouts.app')

@section('title', $title)
@section('subtitle', 'V1.0')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Informations générales</h3>
				</div>
				{!! $form !!}

			</div>
		</div>
	</div>
@endsection