{{ Form::open(array('url' => $url, 'method' => $method, 'files' => true)) }}
<div class="box-body">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group">
        <label for="title">Titre</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Le Titre" value="{{$os->title}}">
    </div>
    <div class="form-group">
        <label for="logo">File input</label>
        <input type="file" id="logo" name="logo">
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Enregistrer</button>
</div>
{{ Form::close() }}