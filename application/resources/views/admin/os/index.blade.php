@extends('Admin::layouts.app')

@push('css')
<link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
<!-- Selece2 -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/select2/select2.min.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datepicker/datepicker3.css') }}">
@endpush

@section('title', 'Liste des Systèmes d\'exploitation')
@section('subtitle', '')


@section('header')

@endsection



@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th></th>
                            <th>Nom</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($operating_systems as $os)
                            <tr>
                                <td>
                                    @if (!is_null($os->logo) && $os->logo != '')
                                        <img width="50px" src="{{asset('storage/'.$os->logo)}}"/>
                                    @endif
                                </td>
                                <td>{{$os->title}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{route('os.edit', ['id' => $os->id])}}"><i
                                                            class="fa fa-edit"></i>Modifier</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="$(this).find('form').submit();">
                                                    {{ Form::open(array('url' => route('os.destroy', ['id' => $os->id]))) }}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::close() }}
                                                    <i class="fa fa-trash-o"></i>Supprimer
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    <!-- /.col -->
    </div>


@endsection
@push('js')
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>
@endpush
