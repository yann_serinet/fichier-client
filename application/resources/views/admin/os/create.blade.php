@extends('Admin::layouts.app')

@push('css')
<link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
<!-- Selece2 -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/select2/select2.min.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datepicker/datepicker3.css') }}">
@endpush

@section('title', 'Ajouter un nouveau système d\'exploitation')
@section('subtitle', 'V1.0')


@section('header')

@endsection



@section('content')

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<!-- /.box-header -->
				<!-- form start -->
			@include('Admin::os._form', ['url' => route('os.store'), 'method' => 'POST'])
			</div>
		</div>
	</div>
@endsection

@push('js')
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>
<!-- FLOT CHARTS -->
<script src="{{ asset('AdminLTE/plugins/flot/jquery.flot.min.js') }}"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="{{ asset('AdminLTE/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('AdminLTE/plugins/select2/select2.full.min.js') }}"></script>

<script src="{{ asset('js/dashboardAdmin.js') }}">
</script>
@endpush
