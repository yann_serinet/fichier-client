@extends('admin.layouts.app')

@section('title', 'Ajouter un nouvel administrateur')
@section('subtitle', 'V1.0')


@section('content')

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<!-- /.box-header -->
				<!-- form start -->
			{!! $default_form !!}
			</div>
		</div>
	</div>
@endsection