@extends('admin.layouts.app')

@section('title', 'Edition du profil administrateur')
@section('subtitle', 'V1.0')

@section('content')

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Informations générales</h3>
				</div>
				{!! $default_form !!}

			</div>
		</div>

		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Changer de mot de passe</h3>
				</div>
				{!! $password_form !!}
			</div>
		</div>
	</div>
@endsection