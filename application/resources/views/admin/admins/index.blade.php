@extends('admin.layouts.app')

@section('title', 'Liste des Admins')
@section('subtitle', '')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Les Administrateurs</h3>
                    <a href="{{route('admin.admins.create')}}" class="pull-right btn btn-success"><i class="fa fa-plus"></i></a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th>email</th>
                            <th>Nom</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($admins as $admin_infos)
                            <tr>
                                <td>{{$admin_infos->email}}</td>
                                <td>{{$admin_infos->name}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{route('admin.admins.edit', ['id' => $admin_infos->id])}}"><i
                                                            class="fa fa-edit"></i>Modifier</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="$(this).find('form').submit();">
                                                    {{ Form::open(array('url' => route('admin.admins.destroy', ['id' => $admin_infos->id]))) }}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::close() }}
                                                    <i class="fa fa-trash-o"></i>Supprimer
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    <!-- /.col -->
    </div>


@endsection
@push('js')
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>
@endpush
