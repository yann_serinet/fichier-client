<html>
<head>

</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#303030" c-style="not3BG" style="background-color: rgb(48, 48, 48);">
    <tbody><tr>
        <td align="center" style="background-image: url('http://rocketway.net/themebuilder/template/templates/notify/images/not3_bg_image.jpg'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #303030;" c-style="not3BG" id="not3">
            <div>
                <!-- Mobile Wrapper -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                    <tbody>
                    <tr>
                        <td width="100%" align="center">
                            <div class="sortable_inner ui-sortable">
                                <!-- Space -->
                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                    <tbody><tr>
                                        <td width="352" height="30"></td>
                                    </tr>
                                    </tbody></table><!-- End Space -->

                                <!-- Space -->
                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                    <tbody><tr>
                                        <td width="352" height="50"></td>
                                    </tr>
                                    </tbody></table><!-- End Space -->
                            </div>

                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" style="border-radius: 5px; box-shadow: rgba(68, 68, 68, 0.2) 0px 0px 7px; background-color: rgb(255, 255, 255);">
                                <tbody><tr>
                                    <td width="352" valign="middle" align="center">


                                        <div class="sortable_inner ui-sortable">
                                            <!-- Start Top -->
                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" style="border-top-left-radius: 5px; border-top-right-radius: 5px; background-color: rgb(255, 255, 255);" object="drag-module-small">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <!-- Header Text -->
                                                        <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody><tr>
                                                                <td width="100%" height="30"></td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody><tr>
                                                                <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" t-style="not3Text" class="fullCenter" object="text-editable">
                                                                    <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>May 2014</singleline><!--[if !mso]><!--></span><!--<![endif]-->
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody><tr>
                                                                <td width="100%" height="40"></td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody><tr>
                                                                <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 43px; color: #3f4345; line-height: 48px;" t-style="not3Headline" class="fullCenter" object="text-editable">
                                                                    <!--[if !mso]><!-->
                                                                    <span style="font-family: 'proxima_novasemibold', Helvetica; font-weight: normal;">
                                                                    <!--<![endif]-->
                                                                        <singleline>Thank you...</singleline>
                                                                    <!--[if !mso]><!-->
                                                                    </span>
                                                                    <!--<![endif]-->
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody>
                                                            <tr>
                                                                <td width="100%" height="45"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td width="100%" height="30"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 30px; color: #3f4345; line-height: 34px;" t-style="not3Text" class="fullCenter" object="text-editable">
                                                                                <!--[if !mso]><!-->
                                                                                <span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;">
                                                                                    <!--<![endif]-->
                                                                                    <singleline>...for being our customer.</singleline>
                                                                                    <!--[if !mso]><!-->
                                                                                </span>
                                                                                <!--<![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">
                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td width="100%" height="30"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" align="center" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                        <tbody>
                                                                        <tr>
                                                                            <td valign="middle" align="center" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" t-style="not3Text" class="fullCenter" object="text-editable">
                                                                                <!--[if !mso]><!-->
                                                                                <span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;">
                                                                                <!--<![endif]-->
                                                                                    <singleline>{{ $message_test }}</singleline>
                                                                                <!--[if !mso]><!-->
                                                                                </span>
                                                                                <!--<![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td width="100%" height="40"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <!----------------- Button Center ----------------->
                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" align="center" bgcolor="#fdba30" c-style="yellowBG" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                    <tbody><tr>
                                                                                        <td align="center" height="40" c-style="notBut1BG" bgcolor="#f0f0f0" style="border-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: rgb(34, 32, 32); background-color: rgb(240, 240, 240);" t-style="notBut1Text">
                                                                                            <multiline><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
																						<a href="#" style="color: #222020; font-size:16px; text-decoration: none; line-height:34px; width:100%;" object="link-editable" t-style="notBut1Text">Emailaddress</a>
                                                                                                    <!--[if !mso]><!--></span><!--<![endif]--></multiline>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!----------------- End Button Center ----------------->

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" align="center" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td width="100%" height="15"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" align="center" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                        <!----------------- Button Center ----------------->
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td align="center" height="40" c-style="notBut2BG" bgcolor="#3f4345" style="border-radius: 20px; padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: rgb(255, 255, 255); background-color: rgb(255, 105, 17);" t-style="notBut2Text">
                                                                                            <multiline>
                                                                                                <!--[if !mso]><!-->
                                                                                                <span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;">
                                                                                                <!--<![endif]-->
                                                                                                    <a href="#" style="color: #ffffff; font-size:16px; text-decoration: none; line-height:34px; width:100%;" t-style="notBut2Text" object="link-editable">Send</a>
                                                                                                <!--[if !mso]><!-->
                                                                                                </span>
                                                                                                <!--<![endif]-->
                                                                                            </multiline>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody>
                                                <tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                            <tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                        <tbody>
                                                                        <tr>
                                                                            <td width="100%" height="40"></td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody><tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" class="pad15" style="background-color: rgb(250, 250, 250);">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                                        <tbody><tr>
                                                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" class="fullCenter" object="text-editable">

                                                                                <multiline>
                                                                                    <!--[if !mso]><!--><span style="font-family: proxima_nova_rgregular, Helvetica; font-weight: normal; color: rgb(63, 67, 69);" t-style="not3Text"><!--<![endif]-->
																					or do something else
                                                                                        <!--[if !mso]><!--></span><!--<![endif]-->

                                                                                    <!--[if !mso]><!--><span style="font-family: 'proxima_novasemibold', Helvetica; font-weight: normal;"><!--<![endif]-->
																				<a href="#" style="color: #3f4345;" t-style="not3Text">here</a>

                                                                                        <!--[if !mso]><!--></span><!--<![endif]-->
                                                                                </multiline>

                                                                            </td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody><tr>
                                                                <td width="265" bgcolor="#fdba30" c-style="yellowBG" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(250, 250, 250);" class="pad15">

                                                                    <table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

                                                                        <tbody><tr>
                                                                            <td width="100%" height="40"></td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" c-style="not3Body" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(255, 255, 255);" object="drag-module-small">
                                                <tbody><tr>
                                                    <td width="352" valign="middle" align="center">

                                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody><tr>
                                                                <td width="265" height="50"></td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                </tbody></table><!-- End Top -->
                                        </div>

                                    </td>
                                </tr>
                                </tbody></table>

                            <div class="sortable_inner ui-sortable">
                                <!-- Space -->
                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                    <tbody><tr>
                                        <td width="352" height="30"></td>
                                    </tr>
                                    </tbody></table><!-- End Space -->
                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                                    <tbody><tr>
                                        <td width="352" valign="middle" align="center">

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                <tbody><tr>
                                                    <td width="352" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;" t-style="unsub2" class="fullCenter">
                                                        <!--[if !mso]><!-->
                                                        <span style="font-family: 'proxima_nova_rgregular', Helvetica;">
                                                            <!--<![endif]-->
                                                            <!--subscribe-->
                                                            <a href="#" style="text-decoration: none; color: #3f4345;" t-style="unsub2" object="link-editable">Unsubscribe</a>
                                                            <!--unsub-->
                                                            <!--[if !mso]><!-->
                                                        </span>
                                                        <!--<![endif]-->
                                                        <span object="text-editable">
                                                            <!--[if !mso]><!-->
                                                            <span style="font-family: 'proxima_nova_rgregular', Helvetica;">
                                                                <!--<![endif]-->
                                                                <singleline>now</singleline>
                                                                <!--[if !mso]><!-->
                                                            </span>
                                                            <!--<![endif]-->
                                                        </span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody></table>
                                <!-- Space -->
                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                    <tbody><tr>
                                        <td width="352" height="50"></td>
                                    </tr>
                                    <tr>
                                        <td width="352" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                    </tr>
                                    </tbody></table><!-- End Space -->
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>