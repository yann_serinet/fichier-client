<!DOCTYPE html>
    <html>
        @include('admin.layouts.partials.head')
        <body class="hold-transition login-page">
            @include('flash::message')
            @yield('content')

            <script src="{{ asset('admin/js/app.js') }}"></script>

            @stack('js')
        </body>
    </html>
