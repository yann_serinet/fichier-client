<!DOCTYPE html>
<html>

    @include('admin.layouts.partials.head')

    <body class="hold-transition login-page">
        @yield('content')

        @yield('js');
    </body>
</html>
