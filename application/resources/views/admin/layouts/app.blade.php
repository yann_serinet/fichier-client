    <!DOCTYPE html>
    <html>
        @include('admin.layouts.partials.head')
        <body class="hold-transition skin-blue sidebar">
            <div class="wrapper">
                @include('admin.layouts.partials.header')
                @include('admin.layouts.partials.sidebar')
                <div class="content-wrapper">

                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            @yield('title')
                            <small>@yield('subtitle')</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">@yield('title')</li>
                        </ol>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        @include('flash::message')

                        @yield('content')
                    </section>
                </div>

                @include('admin.layouts.partials.footer')
            </div>
            <script src="{{ asset('admin/js/app.js') }}"></script>
            @stack('js')

        </body>
    </html>
