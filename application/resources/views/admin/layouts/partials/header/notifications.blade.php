<!-- Notifications: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-success">{{$notifications->count()}}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have {{$notifications->count()}} notifications</li>
        <li>
            <!-- inner menu: contains the actual data -->

            @if($notifications->count() > 0)
            <ul class="menu">

                @foreach($notifications as $notification)
                <li>
                    <a href="#">
                        <i class="fa fa-users text-aqua"></i>{{$notification->title}}
                    </a>
                </li>
                @endforeach

                <li class="footer"><a href="#">View all</a></li>
            </ul>
            @endif
        </li>
    </ul>
</li>