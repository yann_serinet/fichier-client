<header class="main-header">
  <!-- Logo -->
  <a href="{{ url('/') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>TDB</b>DEV</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>TdB Dev</b></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
	    @yield('header')
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ !is_null($admin)?$admin->avatar:'' }}" class="user-image" alt="User Image">
            <span class="hidden-xs">logged : {{ !is_null($admin)?$admin->name:'' }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{ !is_null($admin)?$admin->avatar:'' }}" class="img-circle" alt="User Image">

              <p>
                {{ $admin->name }}
                <small>Membre depuis le {{$admin->created_at->format('d/m/Y')}}</small>
              </p>
            </li>
            <!-- Menu Body -->
            <li class="user-body">
              <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div>
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ route('admin.admins.edit', ['id' => $admin->id]) }}" class="btn btn-default btn-flat">Profil</a>
              </div>
              <div class="pull-right">
                <form action="{{ route('admin.logout') }}" method="POST">
                  <input type="submit" class="btn btn-default btn-flat" value="D&eacute;connexion"/>
                </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
