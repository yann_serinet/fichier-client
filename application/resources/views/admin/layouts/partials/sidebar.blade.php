<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li {!! ViewHelper::isActiveRoute('/', 'class="active"') !!}>
        <a href="{{ route('admin.dashboard.index') }}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li {!! ViewHelper::isActiveRoute('admin.administrateurs.*', 'class="active"') !!}>
        <a href="{{ route('admin.admins.index') }}">
          <i class="fa fa-pie-chart"></i> <span>Administrateurs</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
