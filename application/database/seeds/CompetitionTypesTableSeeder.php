<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompetitionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competition_types')->insert(
            ['title' => 'Championnat d\'Europe']
        );
        DB::table('competition_types')->insert(
            ['title' => 'Championnat du Monde']
        );
        DB::table('competition_types')->insert(
            ['title' => 'Jeux Olympiques']
        );
    }
}
